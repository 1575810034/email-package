<?php

require '../vendor/autoload.php';
/**
 * 发送邮件
 * @param string $subject
 * @param string $from
 * @param string $message
 * @param $to
 * @return bool
 * @throws Exception
 */
function send_email($to, $params = []) {
    try {
//        $expire = $params['expire'] ?? 60; // 发送频率
        $from = $params['from'] ?? ''; // 发送者昵称
        $message = $params['message'] ?? ''; // 正文
        $subject = $params['subject'] ?? ''; // 标题

        //发送邮件通知提醒
        $email = \src\Email::instance();
        $email->subject($subject);
        $from&&$email->from($from);
        if (is_array($to)) {
            foreach ($to as $t) {
                $email->to($t);
            }
        } else {
            $email->to($to);
        }
        $email->message($message);
        $email->send();
        return true;
    } catch (Exception $e) {
        return $e->getMessage();
    }
}


$result = send_email('1575810034@qq.com', [
    'message' => 'hello world',
    'subject' => '测试邮件'
]);
var_dump($result);